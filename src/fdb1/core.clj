(ns fdb1.core
  (:import (com.apple.foundationdb FDB Database)
           (com.apple.foundationdb.tuple Tuple)
           (com.apple.foundationdb.subspace Subspace))
  (:gen-class))

;;;;;

(defn ^java.util.function.Function as-function [f]
  (reify java.util.function.Function
    (apply [this arg] (f arg))))

(defmacro jfn [& args]
  `(as-function (fn ~@args)))

;;;;;

(defn to-tuple [obj]
  (if (coll? obj)
    (Tuple/from (to-array obj))
    (Tuple/from (to-array [obj]))))

;;;; Utility from simple get set

(defn fdb-set
  ([db key val]
   (.run db
         (jfn [tr]
              (let [okey (.pack (to-tuple key))
                    oval (.pack (to-tuple val))]
                (.set tr okey oval)))))
  ([db subspace key val]
   (fdb-set db (.pack subspace key) val)))

(defn fdb-get
  ([db key]
   (.run db
         (jfn [tr]
              (let [okey   (.pack (to-tuple key))
                    aresult (.get tr okey)
                    result (.join aresult)]
                (if (nil? result)
                 nil
                 (first (Tuple/fromBytes result)))))))
  ([db subspace key]
   (fdb-get db (.pack subspace key))))

;;;;; Subspace manipulation

(defn subspace-create
  ([key]
   (let [okey (to-tuple key)]
     (new Subspace okey)))
  ([subspace key]
   (let [okey (to-tuple key)]
     (.subspace subspace okey))))

;;;;;


(def fdb (FDB/selectAPIVersion 600))

(def db (.open fdb  "resources/fdb.cluster"))

(def level1 (subspace-create "level1"))

(def level12 (subspace-create level1 "level2"))
;;;;


(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (println "Hello, World!"))
